import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


def package_files(dir):
    """Get file paths of all files in this directory and its children.

    This recursively walk the directory structure of `dir` and returns the list
    of files in the directory and all of its subdirectories.

    Args:
        dir (str): directory path relatively to `setup.py`

    Returns:
        list: relative file paths in every subdirectory of `dir`
    """
    paths = []
    for (path, _, filenames) in os.walk(dir):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths


data = []
data += package_files('scout_manager/static')
data += package_files('scout_manager/templates')


setup(
    name='scout_manager',
    version='0.0.3',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': data},
    install_requires = [
        'setuptools',
        'Django',
        'django-compressor',
        'django_mobileesp',
        'django-pyscss',
        'pytz',
        'html5lib<=0.9999999',
        'beautifulsoup4',
        'pillow',
        'openpyxl',
        'Django-UserService',
        'Django-SupportTools==1.0',
        'django-pyscss'
    ],
    license='Apache License, Version 2.0',
)
