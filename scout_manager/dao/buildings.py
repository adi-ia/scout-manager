BUILDING_LIST = {
    'Alder Hall (ALD)': {
        'campus': 'seattle'
    },
    'Allen Library (ALB)': {
        'campus': 'seattle'
    },
    'Allen Library (ALB) North': {
        'campus': 'seattle'
    },
    'Allen Library (ALB) South': {
        'campus': 'seattle'
    },
    'Architecture Hall (ARC)': {
        'campus': 'seattle'
    },
    'Art Building (ART)': {
        'campus': 'seattle'
    },
    'Burke Memorial-Washington State Museum (BMM)': {
        'campus': 'seattle'
    },
    'Burke Memorial-Washington State Museum (BMM)': {
        'campus': 'seattle'
    },
    'Business Hall (BHQ)': {
        'campus': 'seattle'
    },
    'Computer Science and Engineering Building (CSE)': {
        'campus': 'seattle'
    },
    'Denny Hall (DEN)': {
        'campus': 'seattle'
    },
    'Electrical Engineering Building (EEB)': {
        'campus': 'seattle'
    },
    'Elm Hall': {
        'campus': 'seattle'
    },
    'Engineering Library (ELB)': {
        'campus': 'seattle'
    },
    'Fishery Sciences (FSH)': {
        'campus': 'seattle'
    },
    'Fluke Hall (FLK)': {
        'campus': 'seattle'
    },
    'Gould Hall (GLD)': {
        'campus': 'seattle'
    },
    'Gowen Hall (GWN)': {
        'campus': 'seattle'
    },
    'Haggett Hall (HGT)': {
        'campus': 'seattle'
    },
    'Hitchcock Hall (HCK)': {
        'campus': 'seattle'
    },
    'Husky Union Building (HUB)': {
        'campus': 'seattle'
    },
    'Hutchinson Hall (HUT)': {
        'campus': 'seattle'
    },
    'Intramural Activities Building (IMA)': {
        'campus': 'seattle'
    },
    'Kane Hall (KNE)': {
        'campus': 'seattle'
    },
    'Lander Hall (LAH)': {
        'campus': 'seattle'
    },
    'Life Sciences Building (LSB)': {
        'campus': 'seattle'
    },
    'Magnuson Health Sciences Center A (HSA)': {
        'campus': 'seattle'
    },
    'Magnuson Health Sciences Center C (HSC)': {
        'campus': 'seattle'
    },
    'Magnuson Health Sciences Center D (HSD)': {
        'campus': 'seattle'
    },
    'Magnuson Health Sciences Center E (HSE)': {
        'campus': 'seattle'
    },
    'Magnuson Health Sciences Center I (HSI)': {
        'campus': 'seattle'
    },
    'Magnuson Health Sciences Center T (HST)': {
        'campus': 'seattle'
    },
    'Maple Hall (MAH)': {
        'campus': 'seattle'
    },
    'Mary Gates Hall (MGH)': {
        'campus': 'seattle'
    },
    'McCarty Hall (MCC)': {
        'campus': 'seattle'
    },
    'McMahon Hall (MCM)': {
        'campus': 'seattle'
    },
    'Mercer Court (MRC)': {
        'campus': 'seattle'
    },
    'Miller Hall (MLR)': {
        'campus': 'seattle'
    },
    'Molecular Engineering & Sciences (MOL)': {
        'campus': 'seattle'
    },
    'Music Building (MUS)': {
        'campus': 'seattle'
    },
    'Odegaard Undergraduate Library (OUGL)': {
        'campus': 'seattle'
    },
    'Paccar Hall (PCAR)': {
        'campus': 'seattle'
    },
    'Padelford Hall (PDL)': {
        'campus': 'seattle'
    },
    'Parrington Hall (PAR)': {
        'campus': 'seattle'
    },
    'Physics/Astronomy Building (PAB) Tower': {
        'campus': 'seattle'
    },
    'Physics / Astronomy Building (PAB)': {
        'campus': 'seattle'
    },
    'Poplar Hall (UTO)': {
        'campus': 'seattle'
    },
    'Red Square': {
        'campus': 'seattle'
    },
    'Roosevelt Clinic': {
        'campus': 'seattle'
    },
    'Science Building (SCI)': {
        'campus': 'tacoma'
    },
    'Smith Hall (SMI)': {
        'campus': 'seattle'
    },
    'Social Work/Speech & Hearing Sciences Building (SWS)': {
        'campus': 'seattle'
    },
    'South Campus Center (SOCC)': {
        'campus': 'seattle'
    },
    'South Campus Center': {
        'campus': 'seattle'
    },
    'Surgery And Treatment Pavilion (UMSP)': {
        'campus': 'seattle'
    },
    'Suzzallo Library (SUZ)': {
        'campus': 'seattle'
    },
    'UW Medical Center': {
        'campus': 'seattle'
    },
    'SLU: Rosen Building (RSN)': {
        'campus': 'seattle'
    },
    'SLU: Brotman Building (BFB)': {
        'campus': 'seattle'
    },
    'SLU: Lab South (SLA)': {
        'campus': 'seattle'
    },
    'SLU: Lab North (SLB)': {
        'campus': 'seattle'
    },
    'SLU: Administration Building (SLC)': {
        'campus': 'seattle'
    },
    'UW Tower Building A (UWTA)': {
        'campus': 'seattle'
    },
    'William H. Foege Genome Sciences (GNOM)': {
        'campus': 'seattle'
    },
    'William H. Foege Bioengineering (BIOE)': {
        'campus': 'seattle'
    },
    'William H. Gates Hall (LAW)': {
        'campus': 'seattle'
    },
    'Willow Hall (WLW)': {
        'campus': 'seattle'
    },



    'Brotman Building (BFB)': {
        'campus': 'south_lake_union'
    },


    'Activities and Recreation Center': {
        'campus': 'bothell'
    },
    'Beardslee Building (UWBB)': {
        'campus': 'bothell'
    },
    'Bothell Bookstore (LB2)': {
        'campus': 'bothell'
    },
    'Bothell Library (LB1)': {
        'campus': 'bothell'
    },
    'Bothell Library Annex (LBA)': {
        'campus': 'bothell'
    },
    'Founders Hall (UW1)': {
        'campus': 'bothell'
    },
    'Chase House': {
        'campus': 'bothell'
    },
    'Commons Hall (UW2)': {
        'campus': 'bothell'
    },
    'Discovery Hall (UW3)': {
        'campus': 'bothell'
    },
    'Husky Hall (HH)': {
        'campus': 'bothell'
    },
    'Husky Village Apartments': {
        'campus': 'bothell'
    },
    'North Creek Events Center': {
        'campus': 'bothell'
    },

    'Truly House': {
        'campus': 'bothell'
    },


    'Academic Building (ADMC)': {
        'campus': 'tacoma'
    },
    'Birmingham Block (BB)': {
        'campus': 'tacoma'
    },
    'Birmingham Hay & Seed (BHS)': {
        'campus': 'tacoma'
    },
    'Carlton Center (CAR)': {
        'campus': 'tacoma'
    },
    'Cherry Parkes (CP)': {
        'campus': 'tacoma'
    },
    'Court 17 Student Housing': {
        'campus': 'tacoma'
    },
    'Dougan (DOU)': {
        'campus': 'tacoma'
    },
    'Garretson Woodruff & Pratt (GWP)': {
        'campus': 'tacoma'
    },
    'Russell T. Joy': {
        'campus': 'tacoma'
    },
    'Joy': {
        'campus': 'tacoma'
    },
    'Keystone (KEY)': {
        'campus': 'tacoma'
    },
    'Laborer\'s Hall': {
        'campus': 'tacoma'
    },
    'Longshoremen\'s Hall (LSH)': {
        'campus': 'tacoma'
    },
    'Mattress Factory (MAT)': {
        'campus': 'tacoma'
    },
    'McDonald Smith (MDS)': {
        'campus': 'tacoma'
    },
    'Pinkerton (PNK)': {
        'campus': 'tacoma'
    },
    'Science (SCI)': {
        'campus': 'tacoma'
    },
    'Snoqualmie Building (SNO)': {
        'campus': 'tacoma'
    },
    'The Whitney': {
        'campus': 'tacoma'
    },
    'Tioga Library Building (TLB)': {
        'campus': 'tacoma'
    },
    'University Y Student Center': {
        'campus': 'tacoma'
    },
    'Walsh Gardner (WG)': {
        'campus': 'tacoma'
    },
    'West Coast Grocery (WCG)': {
        'campus': 'tacoma'
    },
    'William W. Phillip Hall (WPH)': {
        'campus': 'tacoma'
    },

    # UW-Madison Campus Buildings
    '1220 Capitol Ct.': {
        'campus': 'madison'
    },
    '1410 Engineering Dr.': {
        'campus': 'madison'
    },
    '1433 Monroe St.': {
        'campus': 'madison'
    },
    '1610 University Ave.': {
        'campus': 'madison'
    },
    '1645 Linden Dr.': {
        'campus': 'madison'
    },
    '1800 University Ave.': {
        'campus': 'madison'
    },
    '1910 Linden Dr.': {
        'campus': 'madison'
    },
    '206 Bernard Ct.': {
        'campus': 'madison'
    },
    '209 N. Brooks St.': {
        'campus': 'madison'
    },
    '215-217 N. Brooks St.': {
        'campus': 'madison'
    },
    '21 N. Park St.': {
        'campus': 'madison'
    },
    '30 N. Mills St.': {
        'campus': 'madison'
    },
    '333 East Campus Mall': {
        'campus': 'madison'
    },
    '432 East Campus Mall': {
        'campus': 'madison'
    },
    '445 Henry Mall': {
        'campus': 'madison'
    },
    '45 N. Charter St.': {
        'campus': 'madison'
    },
    '502 Herrick Dr.': {
        'campus': 'madison'
    },
    '702 W. Johnson St.': {
        'campus': 'madison'
    },
    '711 State St.': {
        'campus': 'madison'
    },
    '901 University Bay Dr.': {
        'campus': 'madison'
    },
    'Adams Residence Hall': {
        'campus': 'madison'
    },
    'Agricultural Bulletin Building': {
        'campus': 'madison'
    },
    "Agricultural Dean's Residence": {
        'campus': 'madison'
    },
    'Agricultural Engineering Building': {
        'campus': 'madison'
    },
    'Agricultural Engineering Laboratory': {
        'campus': 'madison'
    },
    'Agricultural Hall': {
        'campus': 'madison'
    },
    'Alumni Hall, 1100 Delaplaine Ct.': {
        'campus': 'madison'
    },
    "American Family Children's Hospital": {
        'campus': 'madison'
    },
    'Animal Science Building': {
        'campus': 'madison'
    },
    'Apartment Facilities Office': {
        'campus': 'madison'
    },
    'Armory and Gymnasium (Red Gym)': {
        'campus': 'madison'
    },
    'Art Lofts': {
        'campus': 'madison'
    },
    'Athletics Operations Building': {
        'campus': 'madison'
    },
    'Atmospheric, Oceanic and Space Sciences Building': {
        'campus': 'madison'
    },
    'Babcock Hall': {
        'campus': 'madison'
    },
    'Bardeen Medical Laboritories': {
        'campus': 'madison'
    },
    'Barnard Residence Hall': {
        'campus': 'madison'
    },
    'Bascom Hall': {
        'campus': 'madison'
    },
    'Below Alumni Center': {
        'campus': 'madison'
    },
    "Bernie's Place Childcare": {
        'campus': 'madison'
    },
    'Biotron Laboratory': {
        'campus': 'madison'
    },
    'Birge Hall': {
        'campus': 'madison'
    },
    'Bock Laboratories': {
        'campus': 'madison'
    },
    'Bradley Memorial Building': {
        'campus': 'madison'
    },
    'Bradley Residence Hall': {
        'campus': 'madison'
    },
    'Brogden Psychology Building': {
        'campus': 'madison'
    },
    'Camp Randall Sports Center': {
        'campus': 'madison'
    },
    'Camp Randall Stadium': {
        'campus': 'madison'
    },
    'Carillon Tower': {
        'campus': 'madison'
    },
    'Carl Schuman Shelter': {
        'campus': 'madison'
    },
    'Carson Gulley Center': {
        'campus': 'madison'
    },
    'Cereal Crops Research Unit': {
        'campus': 'madison'
    },
    'Chadbourne Residence Hall': {
        'campus': 'madison'
    },
    'Chamberlin Hall': {
        'campus': 'madison'
    },
    'Chamberlin House (Kronshage)': {
        'campus': 'madison'
    },
    'Charter Street Heating and Cooling Plant': {
        'campus': 'madison'
    },
    'Chazen Museum of Art': {
        'campus': 'madison'
    },
    'Chemistry Building': {
        'campus': 'madison'
    },
    'Cole Residence Hall': {
        'campus': 'madison'
    },
    'Computer Sciences and Statistics': {
        'campus': 'madison'
    },
    'Conover House (Kronshage)': {
        'campus': 'madison'
    },
    'Conrad A. Elvehjem Building': {
        'campus': 'madison'
    },
    'Dairy Barn': {
        'campus': 'madison'
    },
    'Dairy Cattle Center': {
        'campus': 'madison'
    },
    'Davis Residence Hall': {
        'campus': 'madison'
    },
    "D.C. Smith Greenhouse": {
        "campus": "madison"
    },
    "Dejope Residence Hall": {
        "campus": "madison"
    },
    "DeLuca Biochemical Sciences Building": {
        "campus": "madison"
    },
    "DeLuca Biochemistry Building": {
        "campus": "madison"
    },
    "DeLuca Biochemistry Laboratories": {
        "campus": "madison"
    },
    "Discovery Building": {
        "campus": "madison"
    },
    "Eagle Heights": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 101-108": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 201-209": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 301-309": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 401-408": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 501-509": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 601-610": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 701-819": {
        "campus": "madison"
    },
    "Eagle Heights Buildings 901-946": {
        "campus": "madison"
    },
    "Eagle Heights Community Center": {
        "campus": "madison"
    },
    "Educational Sciences": {
        "campus": "madison"
    },
    "Education Building": {
        "campus": "madison"
    },
    "Engineering Centers Building": {
        "campus": "madison"
    },
    "Engineering Hall": {
        "campus": "madison"
    },
    "Engineering Research Building": {
        "campus": "madison"
    },
    "Environmental Health and Safety Building": {
        "campus": "madison"
    },
    "Enzyme Institute": {
        "campus": "madison"
    },
    "Extension Building": {
        "campus": "madison"
    },
    "Field House": {
        "campus": "madison"
    },
    "Fleet and Service Garage": {
        "campus": "madison"
    },
    "Fluno Center For Executive Education": {
        "campus": "madison"
    },
    "Forest Products Laboratory": {
        "campus": "madison"
    },
    "Genetics-Biotechnology Center Building": {
        "campus": "madison"
    },
    "Gilman House (Kronshage)": {
        "campus": "madison"
    },
    "Goodman Softball Complex": {
        "campus": "madison"
    },
    "Goodnight Hall": {
        "campus": "madison"
    },
    "Gordon Dining and Event Center": {
        "campus": "madison"
    },
    "Grainger Hall": {
        "campus": "madison"
    },
    "Gymnasium-Natatorium": {
        "campus": "madison"
    },
    "Hamel Music Center (Under Construction)": {
        "campus": "madison"
    },
    "Hanson Biomedical Sciences Building": {
        "campus": "madison"
    },
    "Harlow Primate Lab": {
        "campus": "madison"
    },
    "Harvey Street Apartments": {
        "campus": "madison"
    },
    "Hasler Laboratory of Limnology": {
        "campus": "madison"
    },
    "Health Sciences Learning Center": {
        "campus": "madison"
    },
    "Helen C. White Hall": {
        "campus": "madison"
    },
    "Hiram Smith Annex": {
        "campus": "madison"
    },
    "Hiram Smith Hall": {
        "campus": "madison"
    },
    "Holt Center (Kronshage)": {
        "campus": "madison"
    },
    "Horse Barn": {
        "campus": "madison"
    },
    "Horticulture": {
        "campus": "madison"
    },
    "Humphrey Hall": {
        "campus": "madison"
    },
    "Ingraham Hall": {
        "campus": "madison"
    },
    "Jones House (Kronshage)": {
        "campus": "madison"
    },
    "Jorns Hall": {
        "campus": "madison"
    },
    "Kellner Hall": {
        "campus": "madison"
    },
    "King Hall": {
        "campus": "madison"
    },
    "Kronshage Residence Hall": {
        "campus": "madison"
    },
    "LaBahn Arena": {
        "campus": "madison"
    },
    "Lathrop Hall": {
        "campus": "madison"
    },
    "Law Building": {
        "campus": "madison"
    },
    "Leopold Residence Hall": {
        "campus": "madison"
    },
    "Livestock Laboratory": {
        "campus": "madison"
    },
    "Lowell Center": {
        "campus": "madison"
    },
    "Mack House (Kronshage)": {
        "campus": "madison"
    },
    "Materials Science and Engineering Building": {
        "campus": "madison"
    },
    "McArdle Cancer Research Building": {
        "campus": "madison"
    },
    "McClain Athletic Facility": {
        "campus": "madison"
    },
    "Meat Science and Muscle Biology Lab": {
        "campus": "madison"
    },
    "Meat Science & Muscle Biology Building 2 (Under Construction)": {
        "campus": "madison"
    },
    "Mechanical Engineering Building": {
        "campus": "madison"
    },
    "Medical Sciences": {
        "campus": "madison"
    },
    "Medical Sciences Center": {
        "campus": "madison"
    },
    "Meiklejohn House": {
        "campus": "madison"
    },
    "Memorial Library": {
        "campus": "madison"
    },
    "Memorial Union": {
        "campus": "madison"
    },
    "Merit Residence Hall": {
        "campus": "madison"
    },
    "Microbial Sciences": {
        "campus": "madison"
    },
    "Middleton Building": {
        "campus": "madison"
    },
    "Moore Hall - Agronomy": {
        "campus": "madison"
    },
    "Mosse Humanities Building": {
        "campus": "madison"
    },
    "Music Hall": {
        "campus": "madison"
    },
    "Nancy Nicholas Hall": {
        "campus": "madison"
    },
    "Nicholas-Johnson Pavilion and Plaza": {
        "campus": "madison"
    },
    "Nielsen Tennis Stadium": {
        "campus": "madison"
    },
    "Noland Zoology Building": {
        "campus": "madison"
    },
    "North Hall": {
        "campus": "madison"
    },
    "Nutritional Sciences": {
        "campus": "madison"
    },
    "Observatory Hill Office Building": {
        "campus": "madison"
    },
    "Ogg Residence Hall": {
        "campus": "madison"
    },
    "Phillips Residence Hall": {
        "campus": "madison"
    },
    "Plant Sciences": {
        "campus": "madison"
    },
    "Police and Security Facility": {
        "campus": "madison"
    },
    "Porter Boathouse": {
        "campus": "madison"
    },
    "Poultry Research Laboratory": {
        "campus": "madison"
    },
    "Pyle Center": {
        "campus": "madison"
    },
    'QQ Express': {
        'campus': 'madison'
    },
    "Radio Hall": {
        "campus": "madison"
    },
    "Rennebohm Hall": {
        "campus": "madison"
    },
    "Russell Laboratories": {
        "campus": "madison"
    },
    "Rust-Schreiner Hall": {
        "campus": "madison"
    },
    "School of Social Work Building": {
        "campus": "madison"
    },
    "Science Hall": {
        "campus": "madison"
    },
    "Seed Building": {
        "campus": "madison"
    },
    "Sellery Residence Hall": {
        "campus": "madison"
    },
    "Service Building": {
        "campus": "madison"
    },
    "Service Building Annex": {
        "campus": "madison"
    },
    "Service Memorial Institute": {
        "campus": "madison"
    },
    "Sewell Social Sciences": {
        "campus": "madison"
    },
    "Showerman House (Kronshage)": {
        "campus": "madison"
    },
    "Signe Skott Cooper Hall": {
        "campus": "madison"
    },
    "Slichter Residence Hall": {
        "campus": "madison"
    },
    "Smith Residence Hall": {
        "campus": "madison"
    },
    "Soils Building": {
        "campus": "madison"
    },
    "Southeast Recreational Facility (SERF)": {
        "campus": "madison"
    },
    "Southeast Residence Halls": {
        "campus": "madison"
    },
    "South Hall": {
        "campus": "madison"
    },
    "Steenbock Library": {
        "campus": "madison"
    },
    "Sterling Hall": {
        "campus": "madison"
    },
    "Stock Pavilion": {
        "campus": "madison"
    },
    "Stovall Building (Wisconsin State Laboratory of Hygiene)": {
        "campus": "madison"
    },
    "Sullivan Residence Hall": {
        "campus": "madison"
    },
    "Swenson House (Kronshage)": {
        "campus": "madison"
    },
    "Taylor Hall": {
        "campus": "madison"
    },
    "Teacher Education": {
        "campus": "madison"
    },
    "The Kohl Center": {
        "campus": "madison"
    },
    "Tripp Residence Hall": {
        "campus": "madison"
    },
    "Turner House (Kronshage)": {
        "campus": "madison"
    },
    "Union South": {
        "campus": "madison"
    },
    "University Club": {
        "campus": "madison"
    },
    "University Hospital": {
        "campus": "madison"
    },
    "University Houses": {
        "campus": "madison"
    },
    "U.S. Dairy Forage Research Center": {
        "campus": "madison"
    },
    "UW Foundation": {
        "campus": "madison"
    },
    "UW Medical Foundation Centennial Building": {
        "campus": "madison"
    },
    "Van Hise Hall": {
        "campus": "madison"
    },
    "Van Vleck Hall": {
        "campus": "madison"
    },
    "Veterans Administration Hospital": {
        "campus": "madison"
    },
    "Veterinary Medicine": {
        "campus": "madison"
    },
    "Vilas Hall": {
        "campus": "madison"
    },
    "Waisman Center": {
        "campus": "madison"
    },
    "Walnut Street Greenhouse": {
        "campus": "madison"
    },
    "WARF Office Building": {
        "campus": "madison"
    },
    "Washburn Observatory": {
        "campus": "madison"
    },
    "Water Science and Engineering Laboratory": {
        "campus": "madison"
    },
    "Waters Residence Hall": {
        "campus": "madison"
    },
    "Weeks Hall for Geological Sciences": {
        "campus": "madison"
    },
    "Wendt Commons": {
        "campus": "madison"
    },
    "West Campus Cogeneration Facility": {
        "campus": "madison"
    },
    "Wisconsin Energy Institute": {
        "campus": "madison"
    },
    "Wisconsin Historical Society": {
        "campus": "madison"
    },
    "Wisconsin Institutes for Medical Research": {
        "campus": "madison"
    },
    "Wisconsin Primate Center": {
        "campus": "madison"
    },
    "Wisconsin Veterinary Diagnostic Laboratory": {
        "campus": "madison"
    },
    "Witte Residence Hall": {
        "campus": "madison"
    },
    "Zoe Bayliss Co-Op": {
        "campus": "madison"
    },
    "Zoology Research Building": {
        "campus": "madison"
    },
}


def get_building_list():
    return flatten_building_list(BUILDING_LIST)


def get_building_list_by_campus(campus):
    buildings = {}
    for building in BUILDING_LIST:
        if BUILDING_LIST[building]['campus'] == campus:
            buildings[building] = BUILDING_LIST[building]
    return flatten_building_list(buildings)


def flatten_building_list(buildings):
    flat_list = {}
    for building in buildings:
        flat_list[building] = buildings[building]['campus']
    return flat_list
