from django.conf import settings
from django.conf.urls import include, url
from django.urls import path
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from scout_manager.views.api import Spot, SpotCreate
from scout_manager.views.api import Item, ItemCreate
from scout_manager.views.pages import items, items_edit, items_add, spaces, \
    spaces_edit, spaces_add, schedule, spaces_upload, image, item_image

urlpatterns = [
    path('', RedirectView.as_view(url='/manager/spaces/')),
    path('items/', items, name='items'),
    path('items/<int:item_id>/', items_edit, name='items_edit'),
    path('items/add/', items_add, name='items_add'),
    path('spaces/', spaces, name='spaces'),
    path('spaces/<int:spot_id>/', spaces_edit, name='spaces_edit'),
    path('spaces/<int:spot_id>/schedule/new/', schedule, name='schedule'),
    # TODO: Why is 20160516 hard-coded here?
    # path('spaces/<int:spot_id>/schedule/20160516/', schedule, name='schedule'),
    path('spaces/add/', spaces_add, name='spaces_add'),
    path('spaces/upload/', spaces_upload, name='spaces_upload'),
    path('api/spot/<int:spot_id>', Spot().run),
    path('api/spot/', SpotCreate().run),
    path('api/item/<int:item_id>', Item().run),
    path('api/item/', ItemCreate().run),
    path('images/<int:spot_id>/image/<int:image_id>/', image, name='manager_image'),
    path('item/images/<int:item_id>/image/<int:image_id>/', item_image, name='manager_item_image'),
]
