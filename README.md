DoIT fork of [scout-manager](https://github.com/uw-it-aca/scout-manager) app by the University of Washington Information Technology's Academic Experience Design & Delivery group.
Major changes are from upstream project is this fork has been updated Python 3.x and Django 2.x.

`scout-manager` is used to populate `scout` app with spot data.

### Development and Configuration
Install scout as a Django module in [WiScout](https://git.doit.wisc.edu/adi-ia/wiscout) Django app.

* Clone WiScout to your machine.

* Install scout-manager as an editable dependency.
  
```sh
# from wiscout directory
$ pipenv install -e /path/to/scout-manager
```



